﻿using UnityEngine;
using System.Collections.Generic;

public class Grid
{
    public List<Cell> cells;

    private int _rowCount;
    private int _columnCount;


    public Grid(int rowCount, int columnCount)
    {
        RowCount = rowCount;
        ColumnCount = columnCount;
        var cellCount = RowCount * ColumnCount;
        cells = new List<Cell>();
        for(int i = 0; i < cellCount; i++)
        {
            cells.Add(new Cell());
        }
    }

    public int RowCount
    {
        get
        {
            return _rowCount;
        }
        private set
        {
            _rowCount = value;
        }
    }

    public int ColumnCount
    {
        get
        {
            return _columnCount;
        }
        private set
        {
            _columnCount = value;
        }
    }


}
