﻿using UnityEngine;
using System.Collections;

public interface IGridPainter
{

    void PaintGrid(Grid grid);
}
