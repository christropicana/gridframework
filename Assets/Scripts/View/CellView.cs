﻿using UnityEngine;

[RequireComponent(typeof(Sprite))]
public class CellView : MonoBehaviour
{
    public CellDimensions dimensions;

    private SpriteRenderer _spriteRenderer;

    void Start()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        DimensionsFromSprite(_spriteRenderer.sprite);
    }

    void DimensionsFromSprite(Sprite sprite)
    {
        var bounds = sprite.bounds;
        dimensions.width = bounds.extents.x * 2;
        dimensions.height = bounds.extents.y * 2;
    } 

    public struct CellDimensions
    {
        public float width;
        public float height;
    }
}