﻿using UnityEngine;
using System.Collections;

public class GridView : MonoBehaviour {

    private Grid _gameGrid;

    public int rowCount;
    public int columnCount;

    private IGridPainter _gridPainter;

    public Vector2 screenDimensions;
    
	// Use this for initialization
	void Start ()
    {
        _gameGrid = new Grid(rowCount, columnCount);
        screenDimensions = new Vector2(Screen.width, Screen.height);
        _gridPainter = GetComponent<IGridPainter>();
        _gridPainter.PaintGrid(_gameGrid);
	}
	
	// Update is called once per frame
	void Update ()
    {
        Debug.Log(screenDimensions);
	}

    
}
