﻿using UnityEngine;
using System.Collections;
using System;

public class TestGridPainter : MonoBehaviour, IGridPainter {

    public GameObject cellPrefab;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void PaintGrid(Grid grid)
    {
        foreach (var cell in grid.cells)
        {
            var go = Instantiate(cellPrefab, Vector2.zero, Quaternion.identity) as GameObject;
            go.transform.parent = this.transform;
        }
    }
}
